package backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class Controlador {

	private String timeZone;
	private String ip;
	private String user;
	private String pass;
	private String db;
	private Connection conector;

	public Controlador(String timeZone, String ip, String db, String user, String pass) {
		super();
		this.timeZone = timeZone;
		this.ip = ip;
		this.user = user;
		this.pass = pass;
		this.db = db;
	}

	public Boolean conectarseADB() {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			// Establecemos la conexi�n con la base de datos.

			this.conector = DriverManager.getConnection(
					"jdbc:mysql://" + this.ip + "/" + this.db + "?serverTimezone=" + this.timeZone, this.user,
					this.pass);

			return true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "EL error fue: " + e.getMessage(), "Error en la conexi�n a la DB",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}

	}

	public ResultSet getItems(int subcaracteristica) {

		// Preparamos la consulta
		Statement s;
		try {
			s = this.conector.createStatement();
			ResultSet rs = s.executeQuery(
					"SELECT c.id AS c_id, c.nombre AS c_nombre, s.id AS s_id, s.nombre as s_nombre, s.descripcion AS s_descripcion, i.descripcion AS i_descripcion, i.puntaje as i_puntaje FROM item i JOIN subcaracteristica s ON i.id_subcaracteristica = s.id JOIN caracteristica c ON c.id = s.id_caracteristica WHERE s.id ="
							+ subcaracteristica);
			// Recorremos el resultado, mientras haya registros para leer, y escribimos el
			// resultado en pantalla.
			return rs;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}
