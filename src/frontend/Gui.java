package frontend;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;

import backend.Controlador;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Gui {

	private JFrame frmAlgoritmoDeCalidad;
	private Controlador controller;
	private ResultSet rsItems;
	private int subcaracteristica = 1;
	private JCheckBox item2;
	private JCheckBox item1;
	private JCheckBox item3;
	private JTextArea txtDescripcion;
	private JLabel lblSubcaracterstica;
	private JLabel lblCaracterstica;
	private JLabel lblCarac;
	private JLabel lblSub;
	private int puntajeTotal = 0;
	private int item1Puntaje = 0;
	private int item2Puntaje = 0;
	private int item3Puntaje = 0;
	private JButton btnSiguiente;
	private JLabel lblOpciones;
	private boolean seleccionarUnaOpcion;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frmAlgoritmoDeCalidad.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		// Intancio el controlador

		this.controller = new Controlador("GMT-3", "127.0.0.1", "qadvance", "qadvance", "qadvance");
		this.controller.conectarseADB();

		frmAlgoritmoDeCalidad = new JFrame();
		frmAlgoritmoDeCalidad.setBackground(new Color(192, 192, 192));
		frmAlgoritmoDeCalidad.getContentPane().setBackground(new Color(192, 192, 192));
		frmAlgoritmoDeCalidad.setTitle("Algoritmo de calidad");
		frmAlgoritmoDeCalidad.setBounds(100, 100, 688, 546);
		frmAlgoritmoDeCalidad.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAlgoritmoDeCalidad.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Algoritmo de calidad");
		lblNewLabel.setForeground(new Color(0, 139, 139));
		lblNewLabel.setFont(new Font("Calibri", Font.BOLD, 27));
		lblNewLabel.setBounds(212, 11, 238, 34);
		frmAlgoritmoDeCalidad.getContentPane().add(lblNewLabel);

		this.lblCaracterstica = new JLabel("Caracter\u00EDstica:");
		lblCaracterstica.setHorizontalAlignment(SwingConstants.CENTER);
		lblCaracterstica.setFont(new Font("Calibri", Font.BOLD, 16));
		lblCaracterstica.setBounds(2, 63, 111, 20);
		frmAlgoritmoDeCalidad.getContentPane().add(lblCaracterstica);

		this.lblSubcaracterstica = new JLabel("Subcaracter\u00EDstica:");
		lblSubcaracterstica.setHorizontalAlignment(SwingConstants.CENTER);
		lblSubcaracterstica.setFont(new Font("Calibri", Font.BOLD, 16));
		lblSubcaracterstica.setBounds(7, 110, 121, 20);
		frmAlgoritmoDeCalidad.getContentPane().add(lblSubcaracterstica);

		this.lblOpciones = new JLabel("Opciones");
		lblOpciones.setForeground(new Color(0, 128, 0));
		lblOpciones.setHorizontalAlignment(SwingConstants.CENTER);
		lblOpciones.setFont(new Font("Calibri Light", Font.BOLD, 17));
		lblOpciones.setBounds(5, 291, 628, 20);
		frmAlgoritmoDeCalidad.getContentPane().add(lblOpciones);

		JPanel pnlContenedorRespuestas = new JPanel();
		pnlContenedorRespuestas.setForeground(new Color(192, 192, 192));
		pnlContenedorRespuestas.setBackground(new Color(192, 192, 192));
		pnlContenedorRespuestas.setBounds(27, 322, 602, 122);
		frmAlgoritmoDeCalidad.getContentPane().add(pnlContenedorRespuestas);
		pnlContenedorRespuestas.setLayout(new GridLayout(3, 1, 0, 0));

		this.item1 = new JCheckBox("");
		item1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				validarUnaOpcion();
			}
		});
		
		item1.setForeground(new Color(0, 0, 0));
		item1.setFont(new Font("Calibri", Font.PLAIN, 15));
		item1.setBackground(new Color(192, 192, 192));
		pnlContenedorRespuestas.add(item1);

		this.item2 = new JCheckBox("");
		item2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				validarUnaOpcion();
			}
		});
		
		item2.setForeground(Color.BLACK);
		item2.setFont(new Font("Calibri", Font.PLAIN, 15));
		item2.setBackground(Color.LIGHT_GRAY);
		pnlContenedorRespuestas.add(item2);

		this.item3 = new JCheckBox("");
		item3.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				validarUnaOpcion();
			}
		});
		
		item3.setForeground(Color.BLACK);
		item3.setFont(new Font("Calibri", Font.PLAIN, 15));
		item3.setBackground(Color.LIGHT_GRAY);
		pnlContenedorRespuestas.add(item3);

		this.txtDescripcion = new JTextArea();
		txtDescripcion.setEditable(false);
		txtDescripcion.setBackground(new Color(192, 192, 192));
		txtDescripcion.setLineWrap(true);
		txtDescripcion.setFont(new Font("Calibri", Font.BOLD, 16));
		txtDescripcion.setBounds(25, 193, 591, 84);
		frmAlgoritmoDeCalidad.getContentPane().add(txtDescripcion);

		this.btnSiguiente = new JButton("SIGUIENTE");
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				avanzar();

			}
		});
		btnSiguiente.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnSiguiente.setBackground(new Color(192, 192, 192));
		btnSiguiente.setBounds(264, 461, 129, 34);
		frmAlgoritmoDeCalidad.getContentPane().add(btnSiguiente);

		JLabel lblDescipcin = new JLabel("Descripci\u00F3n");
		lblDescipcin.setForeground(new Color(0, 128, 0));
		lblDescipcin.setHorizontalAlignment(SwingConstants.CENTER);
		lblDescipcin.setFont(new Font("Calibri", Font.BOLD, 20));
		lblDescipcin.setBounds(249, 150, 129, 31);
		frmAlgoritmoDeCalidad.getContentPane().add(lblDescipcin);

		this.lblCarac = new JLabel("");
		lblCarac.setForeground(new Color(0, 128, 0));
		lblCarac.setHorizontalAlignment(SwingConstants.LEFT);
		lblCarac.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 16));
		lblCarac.setBounds(117, 63, 174, 20);
		frmAlgoritmoDeCalidad.getContentPane().add(lblCarac);

		JLabel label = new JLabel("");
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setFont(new Font("Calibri", Font.PLAIN, 14));
		label.setBounds(170, 180, 208, 20);
		frmAlgoritmoDeCalidad.getContentPane().add(label);

		this.lblSub = new JLabel("");
		lblSub.setForeground(new Color(0, 128, 0));
		lblSub.setHorizontalAlignment(SwingConstants.LEFT);
		lblSub.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 16));
		lblSub.setBounds(134, 111, 283, 20);
		frmAlgoritmoDeCalidad.getContentPane().add(lblSub);

		this.rsItems = this.controller.getItems(this.subcaracteristica);

		try {
			if (rsItems.next()) {
				this.lblCarac.setText(rsItems.getInt(1) + " - " + rsItems.getString(2));
				this.lblSub.setText(rsItems.getString(4));
				this.txtDescripcion.setText(rsItems.getString(5));

				// cargo items
				this.item1.setText(rsItems.getString(6));
				this.item1Puntaje = rsItems.getInt(7);

				if (rsItems.next()) {
					this.item2.setText(rsItems.getString(6));
					this.item2Puntaje = rsItems.getInt(7);
				}

				if (rsItems.next()) {
					this.item3.setText(rsItems.getString(6));
					this.item3.setVisible(true);
					this.item3Puntaje = rsItems.getInt(7);
					this.lblOpciones.setText("Funcionabilidades evaluadas. SELECCIONE UNA OPCI�N");
				} else {
					this.lblOpciones.setText("Funcionabilidades evaluadas. SELECCIONE LAS OPCIONES QUE CORRESPONDAN");
					this.item3.setVisible(false);
					this.item3Puntaje = 0;
				}

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void validarUnaOpcion() {
		if(this.seleccionarUnaOpcion && ((this.item1.isSelected() && this.item2.isSelected()) || (this.item2.isSelected() && this.item3.isSelected())
				|| (this.item1.isSelected() && this.item3.isSelected()))) {
			JOptionPane.showMessageDialog(null, "SOLO PUEDE SELECCIONAR UNA OPCION", "Mas de una opcion seleccionada",
					JOptionPane.WARNING_MESSAGE);
			
			this.item1.setSelected(false);
			this.item2.setSelected(false);
			this.item3.setSelected(false);
		}
		
	}

	protected void avanzar() {
		// TODO Auto-generated method stub
		if (this.subcaracteristica < 12) {
			this.btnSiguiente.setEnabled(true);

			if (this.item1.isSelected())
				this.puntajeTotal += item1Puntaje;
			if (item2.isSelected())
				this.puntajeTotal += item2Puntaje;
			if (item3.isSelected())
				this.puntajeTotal += item3Puntaje;
			
			System.out.println("Puntaje: " + this.puntajeTotal);

			item1.setSelected(false);
			item2.setSelected(false);
			item3.setSelected(false);

			this.subcaracteristica++;

			this.rsItems = this.controller.getItems(this.subcaracteristica);

			try {
				if (rsItems.next()) {
					this.lblCarac.setText(rsItems.getInt(1) + " - " + rsItems.getString(2));
					this.lblSub.setText(rsItems.getString(4));
					this.txtDescripcion.setText(rsItems.getString(5));

					// cargo items
					this.item1.setText(rsItems.getString(6));
					this.item1Puntaje = rsItems.getInt(7);

					if (rsItems.next()) {
						this.item2.setText(rsItems.getString(6));
						this.item2Puntaje = rsItems.getInt(7);
					}

					if (rsItems.next()) {
						this.item3.setText(rsItems.getString(6));
						this.item3.setVisible(true);
						this.item2Puntaje = rsItems.getInt(7);
						this.lblOpciones.setText("Funcionabilidades evaluadas. SELECCIONE UNA OPCI�N");
						this.seleccionarUnaOpcion = true;
					} else {
						this.lblOpciones.setText("Funcionabilidades evaluadas. SELECCIONE LAS OPCIONES QUE CORRESPONDAN");
						this.item3.setVisible(false);
						this.item3Puntaje = 0;
						this.seleccionarUnaOpcion = false;
					}

				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			this.btnSiguiente.setEnabled(false);
			// Satisfactorio
			if (this.puntajeTotal >= 14) {
				JOptionPane.showMessageDialog(null,
						"El software posee una calidad SATISFACTORIA. Puntaje: " + this.puntajeTotal, "SATISFACTORIO!!",
						JOptionPane.INFORMATION_MESSAGE);
			} else { // no satisfactorio
				JOptionPane.showMessageDialog(null,
						"El software tiene una calidad NO satisfactoria. Puntaje:" + this.puntajeTotal,
						"NO SATISFACTORIO", JOptionPane.ERROR_MESSAGE);
			}
			System.exit(1);
		}

	}
}
