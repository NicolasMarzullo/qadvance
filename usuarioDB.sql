GRANT USAGE ON *.* TO 'qadvance'@'%' IDENTIFIED BY PASSWORD '*FA657A67E64F43D7A338FF9334F904AF9F6E004C';

GRANT ALL PRIVILEGES ON `test`.* TO 'qadvance'@'%' WITH GRANT OPTION;

GRANT ALL PRIVILEGES ON `qadvance`.* TO 'qadvance'@'%' WITH GRANT OPTION;