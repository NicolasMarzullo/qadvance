-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-11-2019 a las 18:07:44
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `qadvance`
--
CREATE DATABASE IF NOT EXISTS `qadvance` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `qadvance`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracteristica`
--

CREATE TABLE `caracteristica` (
  `id` smallint(6) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `caracteristica`
--

INSERT INTO `caracteristica` (`id`, `nombre`) VALUES
(1, 'Funcionabilidad'),
(2, 'Confiabilidad'),
(3, 'Usabilidad'),
(4, 'Eficiencia'),
(5, 'Mantenibilidad'),
(6, 'Portabilidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `puntaje` tinyint(4) NOT NULL,
  `id_subcaracteristica` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `item`
--

INSERT INTO `item` (`id`, `descripcion`, `puntaje`, `id_subcaracteristica`) VALUES
(1, 'El sistema posee encriptación de datos', 1, 1),
(2, 'El sistema posee inicio de sesión de usuarios', 1, 1),
(3, 'Los resultados tienen un error en sus resultados a partir del segundo decimal', 0, 2),
(4, 'Los resultados tienen un error en sus resultados a partir del cuarto decimal.', 1, 2),
(5, 'Los resultados tienen un error en sus resultados a partir del sexto decimal.', 2, 2),
(6, 'Cuando sucede un error se protegen los datos procesados.', 1, 3),
(7, 'Se realiza un log de actividades que el sistema estaba haciendo.', 1, 3),
(8, 'Si se produce una falla cualquiera el programa muestra el error, pero no se cierra.', 1, 4),
(9, 'El sistema tiene la posibilidad de realizar backups de la base de datos.', 1, 4),
(10, 'El sistema posee al menos 2 videotutoriales de 2 funciones distintas.', 1, 5),
(11, 'El usuario puede operar con una función determinada luego de ver el videotutorial.', 1, 5),
(12, 'El sistema posee un manual accesible mediante el mismo software.', 1, 6),
(13, 'El manual está escrito en al menos dos idiomas.', 1, 6),
(14, 'En mi equipo consume el 50% de memoria RAM o más.', 0, 7),
(15, 'En mi equipo consume entre el 30% y 49% de memoria RAM.', 1, 7),
(16, 'En mi equipo consume menos de un 30% de memoria RAM.', 2, 7),
(17, 'Tarda menos de 10 segundos en abrir el sistema.', 1, 8),
(18, 'Tarda de 1 a 3 segundos en calcular un pago.', 1, 8),
(19, 'El código posee menos del 20% de comentarios.', 0, 9),
(20, 'El código posee entre 20% - 50% de comentarios.', 1, 9),
(21, 'El código posee más de 50% de comentarios.', 2, 9),
(22, 'La complejidad del método es mayor a 10.', 0, 10),
(23, 'La complejidad del método está entre 7 y 10.', 1, 10),
(24, 'La complejidad del método es menor a 7', 2, 10),
(25, 'El sistema funciona en Windows 10 (32 y 64 bits).', 1, 11),
(26, 'El sistema funciona en Windows 7 (32 y 64 bits)', 1, 11),
(27, 'El software posee una silent install (instalación sin pasos ni ventanas).', 1, 12),
(28, 'El software posee un instalador con instalación básica de hasta 4 pasos.', 1, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcaracteristica`
--

CREATE TABLE `subcaracteristica` (
  `id` smallint(6) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `id_caracteristica` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `subcaracteristica`
--

INSERT INTO `subcaracteristica` (`id`, `nombre`, `descripcion`, `id_caracteristica`) VALUES
(1, 'Seguridad de acceso', 'Capacidad del producto software para asegurar la integridad de los datos y la confidencialidad de estos.', 1),
(2, 'Exactitud de los resultados', 'Es la capacidad del producto software para proporcionar los resultados con el grado necesario de precisión.', 1),
(3, 'Tolerancia a fallos', 'Es la capacidad del producto software de mantener la integridad de los datos cuando se producen fallas del sistema.', 2),
(4, 'Capacidad de recuperación de errores', 'Es la capacidad del sistema de reanudar sus actividades cuando se producen errores críticos.', 2),
(5, 'Capacidad para ser aprendido', 'Mide cuánto tiempo le toma a un usuario aprender a usar una funcionalidad o realizar una tarea específica del software utilizando los videotutoriales.', 3),
(6, 'Capacidad para ser entendido', 'Mide o evalúa cuan comprensible es el sistema y sus respectivas funciones luego de leer el manual y/o el glosario. ', 3),
(7, 'Utilización de recursos', 'Se evaluará la eficiencia del producto software de acuerdo con el porcentaje de uso de procesador que realice.', 4),
(8, 'Comportamiento en el tiempo', 'Se evaluará el tiempo que está el producto software sin informarle al usuario del estado en que se encuentra la solicitud que realizó.', 4),
(9, 'Capacidad del código de ser usado', 'Mide el nivel de entendimiento del método que posea el producto software basándose en el porcentaje de comentarios del código.', 5),
(10, 'Capacidad de ser testeado', 'Evalúa cuan fácil es de testear un método principal según la complejidad ciclomática del método calculado con el método de McCabe.', 5),
(11, 'Adaptabilidad', 'mide la capacidad de hacer funcionar el software en sistemas operativos con distintas arquitecturas y versiones.', 6),
(12, 'Instalabilidad', 'Evalúa la facilidad de instalación del software.', 6);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `caracteristica`
--
ALTER TABLE `caracteristica`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subcaracteristica` (`id_subcaracteristica`);

--
-- Indices de la tabla `subcaracteristica`
--
ALTER TABLE `subcaracteristica`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_caracteritica` (`id_caracteristica`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `fk_subcaracteristica` FOREIGN KEY (`id_subcaracteristica`) REFERENCES `subcaracteristica` (`id`);

--
-- Filtros para la tabla `subcaracteristica`
--
ALTER TABLE `subcaracteristica`
  ADD CONSTRAINT `fk_caracteritica` FOREIGN KEY (`id_caracteristica`) REFERENCES `caracteristica` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
