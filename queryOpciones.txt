INSERT INTO pregunta_funcion VALUES (1,'El sistema posee encriptación de datos',1,1);
INSERT INTO pregunta_funcion VALUES (2,'El sistema posee inicio de sesión de usuarios',1,1);


INSERT INTO pregunta_funcion VALUES (3,'Los resultados tienen un error en sus resultados a partir del segundo decimal',0,2);
INSERT INTO pregunta_funcion VALUES (4,'Los resultados tienen un error en sus resultados a partir del cuarto decimal.',1,2);
INSERT INTO pregunta_funcion VALUES (5,'Los resultados tienen un error en sus resultados a partir del sexto decimal.',2,2);

INSERT INTO pregunta_funcion VALUES(6, 'Cuando sucede un error se protegen los datos procesados.',1,3);
INSERT INTO pregunta_funcion VALUES(7, 'Se realiza un log de actividades que el sistema estaba haciendo.',1,3);


INSERT INTO pregunta_funcion VALUES(8, 'Si se produce una falla cualquiera el programa muestra el error, pero no se cierra.',1,4);
INSERT INTO pregunta_funcion VALUES(9, 'El sistema tiene la posibilidad de realizar backups de la base de datos.',1,4);

INSERT INTO pregunta_funcion VALUES(10, 'El sistema posee al menos 2 videotutoriales de 2 funciones distintas.',1,5);
INSERT INTO pregunta_funcion VALUES(11, 'El usuario puede operar con una función determinada luego de ver el videotutorial.',1,5);

INSERT INTO pregunta_funcion VALUES(12, 'El sistema posee un manual accesible mediante el mismo software.',1,6);
INSERT INTO pregunta_funcion VALUES(13, 'El manual está escrito en al menos dos idiomas.',1,6);

INSERT INTO pregunta_funcion VALUES(14, 'En mi equipo consume el 50% de memoria RAM o más.',0,7);
INSERT INTO pregunta_funcion VALUES(15, 'En mi equipo consume entre el 30% y 49% de memoria RAM.',1,7);
INSERT INTO pregunta_funcion VALUES(16, 'En mi equipo consume menos de un 30% de memoria RAM.',2,7);


INSERT INTO pregunta_funcion VALUES(17, 'Tarda menos de 10 segundos en abrir el sistema.',1,8);
INSERT INTO pregunta_funcion VALUES(18, 'Tarda de 1 a 3 segundos en calcular un pago.',1,8);



INSERT INTO pregunta_funcion VALUES(19, 'El código posee menos del 20% de comentarios.',0,9);
INSERT INTO pregunta_funcion VALUES(20, 'El código posee entre 20% - 50% de comentarios.',1,9);
INSERT INTO pregunta_funcion VALUES(21, 'El código posee más de 50% de comentarios.',2,9);

INSERT INTO pregunta_funcion VALUES(22, 'La complejidad del método es mayor a 10.',0,10);
INSERT INTO pregunta_funcion VALUES(23, 'La complejidad del método está entre 7 y 10.',1,10);
INSERT INTO pregunta_funcion VALUES(24, 'La complejidad del método es menor a 7',2,10);



INSERT INTO pregunta_funcion VALUES(25, 'El sistema funciona en Windows 10 (32 y 64 bits).',1,11);
INSERT INTO pregunta_funcion VALUES(26, 'El sistema funciona en Windows 7 (32 y 64 bits)',1,11);

INSERT INTO pregunta_funcion VALUES(27, 'El software posee una silent install (instalación sin pasos ni ventanas).',1,12);
INSERT INTO pregunta_funcion VALUES(28, 'El software posee un instalador con instalación básica de hasta 4 pasos.',1,12);

"SELECT c.id AS c_id, c.nombre AS c_nombre, s.id AS s_id, s.nombre as s_nombre, s.descripcion AS s_descripcion, i.descripcion AS i_descripcion, i.puntaje as i_puntaje FROM item i JOIN subcaracteristica s ON i.id_subcaracteristica = s.id JOIN caracteristica c ON c.id = s.id_caracteristica"


